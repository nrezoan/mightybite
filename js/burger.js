$('.slide-nav').on('click', function(e) {
  e.preventDefault();
  // get current slide
  var current = $('.flex--active').data('slide'),
    // get button data-slide
    next = $(this).data('slide');

  $('.slide-nav').removeClass('active');
  $(this).addClass('active');

  if (current === next) {
    return false;
  } else {
    $('.slider__warpper').find('.flex__container[data-slide=' + next + ']').addClass('flex--preStart');
    $('.flex--active').addClass('animate--end');
    setTimeout(function() {
      $('.flex--preStart').removeClass('animate--start flex--preStart').addClass('flex--active');
      $('.animate--end').addClass('animate--start').removeClass('animate--end flex--active');
    }, 800);
  }
});
$('.burger_deluxe').on('click',function() {

  $(this).toggleClass('open');
  $('.overlay').toggleClass('open');
  
});
TweenLite.defaultEase = Linear.easeNone;

var cx = 200;
var cy = 200;

var svgns = "http://www.w3.org/2000/svg";
var root = document.querySelector("svg");
var twoPi = Math.PI * 2;
var ease = 0.30;
var total = 20;

var colors = ["#EA4335", "#FBBC05", "#34A853", "#FFD740", "#4285F4", "#E3F2FD", "#827717", "#40C4FF"];

colors.forEach(function (color) {

  var leader = createPoint(color);

  var i = total;
  while (i--) {
    var alpha = (i + 1) / total;
    leader = createLine(leader, alpha, color);
  }
});

TweenLite.from("line, circle", 3.5, { alpha: 0 });

function createPoint(fill) {

  var circle = document.createElementNS(svgns, "circle");
  root.appendChild(circle);

  var radius = random(120, 200);

  TweenLite.set(circle, {
    attr: { r: 2.5, fill: fill },
    x: random(-twoPi, twoPi),
    y: random(-twoPi, twoPi)
  });

  TweenMax.to(circle, random(2, 6), {
    x: "+=" + twoPi,
    repeat: -1,
    modifiers: {
      x: function x(_x) {
        return cx + Math.cos(_x) * radius;
      }
    }
  });

  TweenMax.to(circle, random(2, 6), {
    y: "+=" + twoPi,
    repeat: -1,
    modifiers: {
      y: function y(_y) {
        return cy + Math.sin(_y) * radius;
      }
    }
  });

  return circle._gsTransform;
}

function createLine(leader, alpha, stroke) {

  var line = document.createElementNS(svgns, "line");
  root.appendChild(line);

  TweenLite.set(line, {
    alpha: alpha,
    stroke: stroke,
    x: cx,
    y: cy
  });

  var pos = line._gsTransform;

  TweenMax.to(line, 1000, {
    x: "+=1",
    y: "+=1",
    repeat: -1,
    modifiers: {
      x: function x(_x2) {
        _x2 = pos.x + (leader.x - pos.x) * ease;
        line.setAttribute("x2", leader.x - _x2);
        return _x2;
      },
      y: function y(_y2) {
        _y2 = pos.y + (leader.y - pos.y) * ease;
        line.setAttribute("y2", leader.y - _y2);
        return _y2;
      }
    }
  });

  return pos;
}

function random(min, max) {
  if (max == null) {
    max = min;min = 0;
  }
  return Math.random() * (max - min) + min;
}

